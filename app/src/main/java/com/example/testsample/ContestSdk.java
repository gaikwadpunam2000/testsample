package com.example.testsample;

import android.content.Context;

public class ContestSdk {
    private static Context mContext = null;
    public static  String sdklibraryVersion = "2.5.67";
    public static  Integer sdklibraryIntVersion = 2567;

    public void ContextSdk(Context ctx) {
        this.mContext = ctx;
    }
    public static String getSdkVersion() {
        if (isSDKEnable() == false) return "";
        return sdklibraryVersion;
    }

    public static int getSdkIntVersion() {
        if (isSDKEnable() == false) return 0;
        return sdklibraryIntVersion;
    }

    public static boolean isSDKEnable() {
        boolean sdkStatus = true;
        boolean isSDKDisable = true;
        if (!isSDKDisable)
            sdkStatus = true;

        return sdkStatus;
    }

}
