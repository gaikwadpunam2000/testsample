package com.example.testsample;

import static org.junit.Assert.*;


import android.content.Context;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

//@RunWith(AndroidJUnit4.class);
public class ContestSdkTest {


    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getSdkVersion() {
        String expected = ContestSdk.getSdkVersion();
        String actual = "2.5.67";
        assertEquals(expected, actual);
    }

    @Test
    public void getSdkIntVersion() {
        String expected = String.valueOf(ContestSdk.getSdkIntVersion());
        String actual = "2567";
        assertEquals(expected, actual);
    }

    @Test
    public void isSDKEnable() {
        assertTrue(ContestSdk.isSDKEnable());
    }


    @After
    public void tearDown() throws Exception {
    }

}